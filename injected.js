function handleContextMenu(event) {
	node = event.target;
	while(node != document.body){ 
		if(node.tagName.toLowerCase() == 'a'){
			if(node.hasAttribute('href')){
				safari.self.tab.setContextMenuEventUserInfo(event, node.getAttribute('href'));
				break;
			}
		}
		node = node.parentElement;
	}
}
document.addEventListener("contextmenu", handleContextMenu, false);

