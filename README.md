Show me that feed
=================

A simple Safari extension to display XML in the browser. Just right-click and *Show me that feed*!

Pull requests *very* welcome! Don't be shy to tell me my code is rubbish, I know that already.

Installation
------------

If you're only going to use this extension, just download and install 
[the prepackaged file](https://bitbucket.org/toyg/showmethatfeed/downloads).

Development (or source installation)
------------------------------------

1. git clone git@bitbucket.org:toyg/showmethatfeed.git showmethatfeed.safariextension
1. [Enroll as Safari extension developer](http://developer.apple.com/programs/safari/) (free).
2. [Get a certificate](http://developer.apple.com/certificates/safari/).
3. From Advanced tab in Safari Preferences, check *Show Develop in the menu bar*; 
then from *Develop* menu, select *Show Extension Builder*.
4. click on the *+* icon in lower left corner, select *Add Extension...* and point to the source folder.
5. click on *Install* in the top right corner. Now the extension is installed; 
whenever you want to test a change, click on *Reload* to refresh Safari and all its pages.
